import React, { Component } from 'react';
import User from './User';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username:'',
      password:'',
      isLogin:false
    };
  }
  login(){
    let {username,password} = this.state;
    if(username === "admin" && password === "password"){
      this.setState({isLogin:true})
    }
  }
  render() {
    if(this.state.isLogin){
      return <User username={this.state.username}/>
    }
    return (
      <div className="App">
        <h2>Login</h2>
        <div className="input-field">User name <input value={this.state.username} onChange={ (e) => this.setState({username:e.target.value})}/></div>
        {this.state.username === "admin" ? <div className="input-field">Password <input value={this.state.password} type="password" onChange={ (e) => this.setState({password:e.target.value})}/></div>:null}
        <div className="input-field"><input type="button" value="Login" onClick={ (e)=> this.login()}/></div>

      </div>
    );
  }
}

export default App;
